#!/usr/bin/env python3
import logging

from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv

from satnogs_api_client import fetch_satellites, DB_BASE_URL
from satellite_tle import fetch_tles


if __name__ == '__main__':
    # Setting logging level
    logging.basicConfig(level=logging.INFO,
                        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")


    print('Fetch the satellites of interest from satnogs-db.', flush=True)
    sats = fetch_satellites(url=DB_BASE_URL, max_satellites=None)
    satnogs_db_norad_ids = set(sat['norad_cat_id'] for sat in sats if sat['status'] != 're-entered')

    # Remove satellites with temporary norad ids
    temporary_norad_ids = set(filter(lambda norad_id: norad_id >= 99900, satnogs_db_norad_ids))
    satnogs_db_norad_ids = satnogs_db_norad_ids - temporary_norad_ids

    # Fetch TLEs for the satellites of interest
    tles = fetch_tles(satnogs_db_norad_ids)
    missing_norad_ids = satnogs_db_norad_ids - set(tles.keys())

    # Print information on the TLEs found
    print("\nList of all TLEs found:")
    for norad_id, (source, tle) in tles.items():
        sat = twoline2rv(tle[1], tle[2], wgs72)
        print('{:5d} {:23s}: {:23s}, {:%Y-%m-%d %H:%M}'.format(norad_id, tle[0], source, sat.epoch))

    # List satellites with missing TLEs
    still_missing_norad_ids = set(satnogs_db_norad_ids) - set(tles.keys())
    sats_missing = list(sat for sat in sats if sat['norad_cat_id'] in still_missing_norad_ids)
    sats_missing.sort(key=lambda sat: int(sat['norad_cat_id']))
    sats_temporary = list(sat for sat in sats if sat['norad_cat_id'] in temporary_norad_ids)
    sats_temporary.sort(key=lambda sat: int(sat['norad_cat_id']))

    if len(temporary_norad_ids) > 0:
        print('\nList of Satellites with temporary norad id assignment (norad_id >= 99900):')
        print(', '.join(list('{} ({})'.format(sat['norad_cat_id'], sat['name']) for sat in sats_temporary)))
    if len(sats_missing) > 0:
        print('\nList of Satellites without a TLE source:')
        print(', '.join(list('{} ({})'.format(sat['norad_cat_id'], sat['name']) for sat in sats_missing)))

    # Print final status message
    print('\nTLEs for {} of {} requested satellites found ({} satellites with temporary norad ids skipped).'.format(len(tles), len(satnogs_db_norad_ids), len(temporary_norad_ids)))
